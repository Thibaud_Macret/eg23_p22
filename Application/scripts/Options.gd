extends Node2D

var valeur:int = 5 #valeur du son

func _on_Button_pressed():
	valeur += 1
	valeur = min(valeur, 10)
	actualiser()

func _on_Button2_pressed():
	valeur -= 1
	valeur = max(valeur, 0)
	actualiser()

func actualiser():
	$Son/ColorRect.rect_size.x = valeur * 36.8

func _on_BtnReturn_pressed():
	get_tree().change_scene("res://scenes/MainMenu.tscn")
