extends Node2D

# Menu de création de la partie

var playerSprites:Array = [
preload("res://imgs/PP_Joueur0.png"),
preload("res://imgs/PP_Joueur1.jpg"),
preload("res://imgs/PP_Joueur2.jpg"),
preload("res://imgs/PP_Joueur3.jpg"),
preload("res://imgs/PP_Joueur4.jpg")
] # Le tableau contient les images pour les joueurs

func _on_BtnJouer_pressed():
	get_tree().change_scene("res://scenes/PreparationMenu.tscn")

func _on_BtnOptions_pressed():
	get_tree().change_scene("res://scenes/Options.tscn")

func _on_BtnCredits_pressed():
	get_tree().change_scene("res://scenes/Credits.tscn")

func _on_BtnQuitter_pressed():
	get_tree().quit()


var colors:Array = [
	Color("ff0000"),
	Color("0000ff"),
	Color("7e007e"),
	Color("ff7e00"),
	Color("007e00"),
	Color("00fff9")
] # Le tableau contient les couleurs pour les joueurs

# Les parties Player 1 et 2 sont identiques mais ne ciblent pas les même variables

#####Player 1#####
func _on_TxtEditName_text_changed(): #Changement du nom
	if $Player1/TxtEditName.text.length() > 16:
		$Player1/TxtEditName.text = Globals.player1Name
	else :
		Globals.player1Name = $Player1/TxtEditName.text

func _on_ColorRect_gui_input(event): #Apparition du choix de couleur
	if Input.is_action_just_pressed("click"):
		$Player1/MyColorPicker.visible = !$Player1/MyColorPicker.visible

#Les 6 suivantes correspondent aux 6 couleurs (fonction non factorisée)
func _on_Color1_pressed():
	Globals.player1Color = colors[0]
	$Player1/MyColorPicker.visible = false
	$Player1/ColorBtn.color = Globals.player1Color

func _on_Color2_pressed():
	Globals.player1Color = colors[1]
	$Player1/MyColorPicker.visible = false
	$Player1/ColorBtn.color = Globals.player1Color

func _on_Color3_pressed():
	Globals.player1Color = colors[2]
	$Player1/MyColorPicker.visible = false
	$Player1/ColorBtn.color = Globals.player1Color

func _on_Color4_pressed():
	Globals.player1Color = colors[3]
	$Player1/MyColorPicker.visible = false
	$Player1/ColorBtn.color = Globals.player1Color

func _on_Color5_pressed():
	Globals.player1Color = colors[4]
	$Player1/MyColorPicker.visible = false
	$Player1/ColorBtn.color = Globals.player1Color

func _on_Color6_pressed():
	Globals.player1Color = colors[5]
	$Player1/MyColorPicker.visible = false
	$Player1/ColorBtn.color = Globals.player1Color

#choix de l'image
var playerSprite1:int = 0 
func _on_SelectPrevious_input_event(viewport, event, shape_idx):
	if Input.is_action_just_pressed("click"):
		playerSprite1 -= 1
		if(playerSprite1 < 0):
			playerSprite1 = 4
		actualise_sprite()

func _on_SelectNext_input_event(viewport, event, shape_idx):
	if Input.is_action_just_pressed("click"):
		playerSprite1 += 1
		if(playerSprite1 > 4):
			playerSprite1 = 0
		actualise_sprite()

func actualise_sprite():
	$Player1/Sprite.set_texture(playerSprites[playerSprite1])

func _on_TxtEditName_gui_input(event):
	if Input.is_action_just_pressed("click"):
		$Player1/TxtEditName.text = ""



#####Player 2#####
var playerSprite2:int = 0
func _on_TxtEditName_text_changed_2():
	if $Player2/TxtEditName.text.length() > 16:
		$Player2/TxtEditName.text = Globals.player2Name
	else :
		Globals.player2Name = $Player2/TxtEditName.text

func _on_ColorRect_gui_input_2(event):
	if Input.is_action_just_pressed("click"):
		$Player2/MyColorPicker.visible = !$Player2/MyColorPicker.visible

func _on_Color1_pressed_2():
	Globals.player2Color = colors[0]
	$Player2/MyColorPicker.visible = false
	$Player2/ColorBtn.color = Globals.player2Color

func _on_Color2_pressed_2():
	Globals.player2Color = colors[1]
	$Player2/MyColorPicker.visible = false
	$Player2/ColorBtn.color = Globals.player2Color

func _on_Color3_pressed_2():
	Globals.player2Color = colors[2]
	$Player2/MyColorPicker.visible = false
	$Player2/ColorBtn.color = Globals.player2Color

func _on_Color4_pressed_2():
	Globals.player2Color = colors[3]
	$Player2/MyColorPicker.visible = false
	$Player2/ColorBtn.color = Globals.player2Color

func _on_Color5_pressed_2():
	Globals.player2Color = colors[4]
	$Player2/MyColorPicker.visible = false
	$Player2/ColorBtn.color = Globals.player2Color

func _on_Color6_pressed_2():
	Globals.player2Color = colors[5]
	$Player2/MyColorPicker.visible = false
	$Player2/ColorBtn.color = Globals.player2Color

func _on_CheckBox_pressed():
	$Player2/ZoneIA/TypeIA.visible = $Player2/ZoneIA/CheckBox.pressed

func _on_SelectPrevious2_input_event_2(viewport, event, shape_idx):
	if Input.is_action_just_pressed("click"):
		playerSprite2 -= 1
		if(playerSprite2 < 0):
			playerSprite2 = 4
		actualise_sprite2()

func _on_SelectNext2_input_event_2(viewport, event, shape_idx):
	if Input.is_action_just_pressed("click"):
		playerSprite2 += 1
		if(playerSprite2 > 4):
			playerSprite2 = 0
		actualise_sprite2()

func actualise_sprite2():
	$Player2/Sprite.set_texture(playerSprites[playerSprite2])

func _on_TxtEditName_gui_input2(event):
	if Input.is_action_just_pressed("click"):
		$Player2/TxtEditName.text = ""
