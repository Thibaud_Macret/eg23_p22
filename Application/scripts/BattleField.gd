extends Node2D

# Le champ de bataille

var soldats:Array # Les soldats sont importés de Globals, pour garder un côté logique
var soldatSelect:int = 0 # Le soldat sélectionné
var lieuxSoldats:Array = [1, 1, 1, 1, 2, 3, 1, 0, 1, 0, 2, 3, 1, 1, 1, 1, 2, 3, 4, 4]  # Le lieux de chaque soldat

func _ready():
	$LblJoueur.text = Globals.player1Name
	$LblJoueur.add_color_override("font_color", Globals.player1Color)
	soldats = Globals.troupes

func _on_BtnQuestion_pressed():
	$PannelAide.visible = true

func _on_BtnQuestionFermer_pressed():
	$PannelAide.visible = false

func _on_Perso_mouse_entered(id):
	$Equipe/Stats/Nom.text = soldats[id][0]
	for index in range (1,6):
		$Equipe/Stats.get_child(index).get_child(0).get_child(0).text = String(soldats[id][1+index])
		if index == 4:
			$Equipe/Stats.get_child(index).get_child(0).rect_size.x = 28 * soldats[id][1+index] / 3
		else :
			$Equipe/Stats.get_child(index).get_child(0).rect_size.x = 28 * soldats[id][1+index]

func _on_BtnEquipe_pressed():
	$Camera2D.current = false
	$Equipe/Camera2D.current = true
	_on_Perso_mouse_entered(0)

func _on_BtnOptions_pressed():
	get_tree().change_scene("res://scenes/OptionsIG.tscn")

func _on_Return_input_event(viewport, event, shape_idx):
	if Input.is_action_just_pressed("click"):
		$Equipe/Camera2D.current = false
		$Camera2D.current = true


var persoSelectionne:int = -1

func _on_Soldat_input_event(viewport, event, shape_idx, id): # Selection d'un soldat
	if Input.is_action_just_pressed("click"):
		if(persoSelectionne != -1):
			$Persos.get_child(persoSelectionne).get_child(0).visible = false
		persoSelectionne = id
		$Persos.get_child(persoSelectionne).get_child(0).visible = true

func _on_Soldat_mouse_entered(id): # Au survol, indique le lieu
	for index in range (5):
		$UTT_battlefieds.get_child(index).get_child(1).add_color_override("font_color", "#000000")
	if(lieuxSoldats[id] != -1):
		$UTT_battlefieds.get_child(lieuxSoldats[id]).get_child(1).add_color_override("font_color", "#a72300")

func _on_Soldat_mouse_exited(id):
	for index in range (5):
		$UTT_battlefieds.get_child(index).get_child(1).add_color_override("font_color", "#000000")


func _on_UTT_battlefield_input_event(viewport, event, shape_idx, id): # Envoie le perso sélectionné dans le lieu
	if(persoSelectionne != -1) && Input.is_action_just_pressed("click"):
		$Persos.get_child(persoSelectionne).get_child(0).visible = false
		lieuxSoldats[persoSelectionne] = id
		persoSelectionne = -1
		_on_UTT_battlefield_mouse_entered(id)

func _on_UTT_battlefield_mouse_entered(id): # Pour chaque perso, regarde s'il est dans le lieu et le met en visible si c'est le cas
	for index in range (20):
		$Persos.get_child(index).get_child(1).visible = (lieuxSoldats[index] == id)

func _on_UTT_battlefield_mouse_exited(id):
	for index in range (20):
		$Persos.get_child(index).get_child(1).visible = false
