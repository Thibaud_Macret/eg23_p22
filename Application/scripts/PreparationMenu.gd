extends Node2D

var pointsRestants:int = 400
var timer:float = 0 # Le timer est utilisé pour bloquer le "spam click"
var nbReservistes:int = 5

var soldats:Array = [
	["Soldat 01", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 02", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 03", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 04", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 05", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 06", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 07", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 08", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 09", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 10", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 11", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat 12", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat 13", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat 14", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat 15", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat elite 01", preload("res://imgs/soldat__elite.png"), 2, 2, 2, 5, 1, false, 2],
	["Soldat elite 02", preload("res://imgs/soldat__elite.png"), 2, 2, 2, 5, 1, false, 2],
	["Soldat elite 03", preload("res://imgs/soldat__elite.png"), 2, 2, 2, 5, 1, false, 2],
	["Soldat elite 04", preload("res://imgs/soldat__elite.png"), 2, 2, 2, 5, 1, false, 2],
	["Maître de guerre", preload("res://imgs/maitredeguerre.png"), 3, 3, 3, 10, 3, false, 2]
]
var soldatSelect:int = 0

func _ready():
	$LblJoueur.text = Globals.player1Name
	$LblJoueur.add_color_override("font_color", Globals.player1Color)
	actu_labels()
	actu_soldat()

func actu_labels():
	$PtsRestants/Label.text = String(pointsRestants) + " points restants"
	$PtsRestants/Avant.rect_size.x = pointsRestants * 0.32


func _on_SelectPrevious_input_event(viewport, event, shape_idx): # Change le soldat
	if Input.is_action_just_pressed("click") && timer == 0:
		soldatSelect -= 1
		if(soldatSelect < 0):
			soldatSelect = 19
		timer = .1
		actu_soldat()

func _on_SelectNext_input_event(viewport, event, shape_idx): # Change le soldat
	if Input.is_action_just_pressed("click") && timer == 0:
		soldatSelect += 1
		if(soldatSelect > 19):
			soldatSelect = 0
		timer = .1
		actu_soldat()

func _physics_process(delta):
	timer = max(timer - delta, 0)

func actu_soldat(): # Actualise les éléments en fonction du soldat choisi
	$Image/Sprite.set_texture(soldats[soldatSelect][1])
	$TxtEditName.text = soldats[soldatSelect][0]
	for index in range(2,7):
		actualiser(index)
	$Reserviste.pressed = soldats[soldatSelect][7]
	$IA.get_child(soldats[soldatSelect][8]).pressed = true

func _on_TxtEditName_text_changed(): # Changement du nom du soldat
	if $TxtEditName.text.length() > 14:
		$TxtEditName.text = soldats[soldatSelect][0]
	else :
		soldats[soldatSelect][0] = $TxtEditName.text


func _on_BtnQuestion_pressed():
	$PannelAide.visible = true

func _on_BtnQuestionFermer_pressed():
	$PannelAide.visible = false
	
func _on_BtnJouer_pressed(): # Lancement de la partie (ssi il y a 5 reservistes)
	if(pointsRestants == 0):
		jouer()
	elif(pointsRestants > 0) :
		$PopupPoints/Label.text = "Il vous reste des points, voulez vous les assigner automatiquement ?"
		$PopupPoints.visible = true
		$PopupPoints/BtnJouerConfirm.visible = true
		$PopupPoints/BtnJouerRetour.visible = true
		$PopupPoints/BtnRetour.visible = false
	else :
		$PopupPoints/Label.text = "Vous avez dépensé trop de points !"
		$PopupPoints.visible = true
		$PopupPoints/BtnJouerConfirm.visible = false
		$PopupPoints/BtnJouerRetour.visible = false
		$PopupPoints/BtnRetour.visible = true


func _on_Avant_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(1, id)

func _on_Avant2_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(2, id)

func _on_Avant3_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(3, id)

func _on_Avant4_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(4, id)

func _on_Avant5_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(5, id)

func _on_Avant6_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(6, id)

func _on_Avant7_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(7, id)

func _on_Avant8_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(8, id)

func _on_Avant9_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(9, id)

func _on_Avant10_gui_input(event, id): # Un clique sur la barre fixe la valeur
	if Input.is_action_just_pressed("click"):
		appui_barre(10, id)

func appui_barre(valeur, id):
	if(id == 5):
		valeur *= 3
	pointsRestants += soldats[soldatSelect][id] - valeur
	soldats[soldatSelect][id] = valeur
	actualiser(id)

func _on_Plus_input_event(viewport, event, shape_idx, id): # plus un de stat
	if Input.is_action_just_pressed("click"):
		if!(soldats[soldatSelect][id] == (10 if id!=5 else 30)):
			soldats[soldatSelect][id] += 1
			pointsRestants -=1
			actualiser(id)

func _on_Minus_input_event(viewport, event, shape_idx, id): # plus un de stat
	if Input.is_action_just_pressed("click"):
		if!(soldats[soldatSelect][id] == 0):
			soldats[soldatSelect][id] -= 1
			pointsRestants +=1
			actualiser(id)

func actualiser(id:int): # Actualise la barre [id]
	$Stats.get_child(id).get_child(1).text = String(soldats[soldatSelect][id])
	if(id != 5):
		for index in range (10):
			$Stats.get_child(id).get_child(4).get_child(index).visible = index < soldats[soldatSelect][id]
	else :
		for index in range (10):
			$Stats.get_child(id).get_child(4).get_child(index).visible = index < (soldats[soldatSelect][id]/3)
	actu_labels()


func _on_Reserviste_pressed(): # Met en reserviste
	soldats[soldatSelect][7] = $Reserviste.pressed
	if $Reserviste.pressed :
		nbReservistes += 1
	else :
		nbReservistes -= 1
	$BtnJouerBlock.visible = nbReservistes != 5
	$BtnJouerBlock/Label.text = "Vous devez avoir 5 reservistes ! (" + String(nbReservistes) + ")"


func _on_CheckBox_pressed(): # Change l'IA
	soldats[soldatSelect][8] = 0

func _on_CheckBox2_pressed():
	soldats[soldatSelect][8] = 1

func _on_CheckBox3_pressed():
	soldats[soldatSelect][8] = 2

###Menu Equipe###

func _on_Perso_mouse_entered(id): # Au survol : montre les stats
	$Equipe/Stats/Nom.text = soldats[id][0]
	for index in range (1,6):
		$Equipe/Stats.get_child(index).get_child(0).get_child(0).text = String(soldats[id][1+index])
		if index == 4:
			$Equipe/Stats.get_child(index).get_child(0).rect_size.x = 28 * soldats[id][1+index] / 3
		else :
			$Equipe/Stats.get_child(index).get_child(0).rect_size.x = 28 * soldats[id][1+index]
	$Equipe/Stats/Reserviste.visible = soldats[id][7]

func _on_BtnEquipe_pressed(): # Au click : retour sur le menu de stats
	$Camera2D.current = false
	$Equipe/Camera2D.current = true
	_on_Perso_mouse_entered(0)

func _on_Perso_input_event(viewport, event, shape_idx, id):
	if Input.is_action_just_pressed("click") :
		soldatSelect = id
		actu_soldat()
		$Equipe/Camera2D.current = false
		$Camera2D.current = true

#Gestion des points pour lancer la partie

func _on_BtnJouerRetour_pressed():
	$PopupPoints.visible = false

func _on_BtnJouerConfirm_pressed():
	jouer()

func jouer():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/BattleField.tscn")
	Globals.troupes = soldats


func _on_BtnOptions_pressed():
	get_tree().change_scene("res://scenes/OptionsPrep.tscn")
