extends Node2D

func _on_BtnJouer_pressed():
	get_tree().change_scene("res://scenes/GameCreationMenu.tscn")

func _on_BtnOptions_pressed():
	get_tree().change_scene("res://scenes/Options.tscn")

func _on_BtnCredits_pressed():
	get_tree().change_scene("res://scenes/Credits.tscn")

func _on_BtnQuitter_pressed():
	get_tree().quit()
