extends Node

# Globals contient les variables accessibles depuis toutes les scènes
# Comme ce n'est qu'une maquette, tout n'est pas sauvegardé

var player1Name:String = "Joueur1"
var player1Color:Color = Color("ff0000")
var player2Name:String = "Joueur2"
var player2Color:Color = Color("0000ff")

var troupes:Array = [
	["Soldat 01", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 02", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 03", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 04", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 05", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 06", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 07", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 08", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 09", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 10", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, false, 2],
	["Soldat 11", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat 12", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat 13", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat 14", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat 15", preload("res://imgs/soldat.png"), 1, 1, 1, 0, 1, true, 2],
	["Soldat elite 01", preload("res://imgs/soldat__elite.png"), 2, 2, 2, 5, 1, false, 2],
	["Soldat elite 02", preload("res://imgs/soldat__elite.png"), 2, 2, 2, 5, 1, false, 2],
	["Soldat elite 03", preload("res://imgs/soldat__elite.png"), 2, 2, 2, 5, 1, false, 2],
	["Soldat elite 04", preload("res://imgs/soldat__elite.png"), 2, 2, 2, 5, 1, false, 2],
	["Maître de guerre", preload("res://imgs/maitredeguerre.png"), 3, 3, 3, 10, 3, false, 2]
]
